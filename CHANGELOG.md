v2.0.1
- Removed test commands from the published module.

v2.0.0
- Rewrite of the module. Existing functionality should still work. Please report issues by opening a ticket on the new [GitLab repository](https://gitlab.com/woodentavern/foundryvtt-chat-command-lib/-/issues).
- Renamed the module to **Chat Commander** to reflect the new user facing features (see below).
- Added autocompletion functionality for whisper and roll commands.
- Deprecated old properties with some functional changes. You will receive warnings for now, but they will be removed in a future version.
    - The `commandKey` is now called `name`.
    - The `iconClass` property is now called `icon` and should contain the full HTML of an image or icon.
    - The `invokeOnCommand` function is now called `callback` and can return a message data object that will be merged with the data of the original message. If an empty object is returned, no message will be sent. Returning null will apply FoundryVTT core command handling.
    - The properties `shouldDisplayToChat` and `createdMessageType` no longer exist (see above). Instead, your callback should return a message data object or `{}` to omit the message.
    - The `gmOnly` flag is now called `requiredRole` and should contain the name of a minimum role (see FoundryVTT's [user roles](https://foundryvtt.com/api/enums/foundry.CONST.USER_ROLES.html).
    - The `registerCommand` method is now called `register`.
    - The `deregisterCommand` method is now called `unregister`.
- Added new developer features:
    - Added `aliases` to specify additional command names that will use the same logic.
    - Added `autocompleteCallback` to suggest command parameters to the user. This can be used to complete the command (see `ChatCommands.createCommandElement`) and/or to display additional information (see `ChatCommands.createInfoElement`). Check out the [core command implementations](src/scripts/coreCommands.mjs) for examples. By default, the result should be an array of `HTMLElement`s (or an `HTMLCollection`) that the module will limit to a user configurable amount of entries. You may also return an asynchronous generator for more complex scenarios (check the development section of the [README](README.md#autocomplete-callback) for details).
    - Added an `invokeChatCommand` hook that is called after the execution of a command, but before its result is sent to the chat. It can be used to react to commands or modify the resulting message.
    - Command invocations may now be asynchronous (and still return message data).
- Added localization and German translation. Feel free to contribute additional languages with a pull request or by messaging me on Discord (`@DJ Addi`).
- Fixed compatibility with "Autocomplete Whisper", the "Das Schwarze Auge 5" system and similar chat menu implementations.


v1.4.0 and below

Check [the old module page](https://github.com/League-of-Foundry-Developers/Chat-Commands-Lib).