## Classes

<dl>
<dt><a href="#AutocompleteMenu">AutocompleteMenu</a></dt>
<dd><p>Extension for the chat text input to enable autocomplete functionalities.</p>
</dd>
<dt><a href="#ChatCommand">ChatCommand</a></dt>
<dd><p>Represents a single chat command.</p>
</dd>
<dt><a href="#ChatCommands">ChatCommands</a></dt>
<dd><p>Registry for chat commands and utility methods.</p>
</dd>
</dl>

<a name="AutocompleteMenu"></a>

## AutocompleteMenu
Extension for the chat text input to enable autocomplete functionalities.

**Kind**: global class  

* [AutocompleteMenu](#AutocompleteMenu)
    * [new AutocompleteMenu(chatInput)](#new_AutocompleteMenu_new)
    * _instance_
        * [.maxEntries](#AutocompleteMenu+maxEntries) : <code>number</code>
        * [.showFooter](#AutocompleteMenu+showFooter) : <code>boolean</code>
        * [.currentCommand](#AutocompleteMenu+currentCommand) : [<code>ChatCommand</code>](#ChatCommand)
        * [.open(text)](#AutocompleteMenu+open)
        * [.display(entries, [append], [done])](#AutocompleteMenu+display)
        * [.focus(event)](#AutocompleteMenu+focus)
        * [.blur()](#AutocompleteMenu+blur)
        * [.navigate(event)](#AutocompleteMenu+navigate)
        * [.close()](#AutocompleteMenu+close)
        * [.suggest(command)](#AutocompleteMenu+suggest)
        * [.select(command)](#AutocompleteMenu+select)
        * [.resetSuggestion()](#AutocompleteMenu+resetSuggestion)
    * _static_
        * [.initialize()](#AutocompleteMenu.initialize)
        * [.stopEvent(event)](#AutocompleteMenu.stopEvent)

<a name="new_AutocompleteMenu_new"></a>

### new AutocompleteMenu(chatInput)
Creates a new menu and registers listeners to enable autocompletion of commands.


| Param | Type | Description |
| --- | --- | --- |
| chatInput | <code>HTMLTextAreaElement</code> | The text area to attach the listeners to. |

<a name="AutocompleteMenu+maxEntries"></a>

### autocompleteMenu.maxEntries : <code>number</code>
Maximum amount of entries that can be displayed in the menu.

**Kind**: instance property of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
<a name="AutocompleteMenu+showFooter"></a>

### autocompleteMenu.showFooter : <code>boolean</code>
Indicates whether the command footer should be displayed.

**Kind**: instance property of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
<a name="AutocompleteMenu+currentCommand"></a>

### autocompleteMenu.currentCommand : [<code>ChatCommand</code>](#ChatCommand)
Stores the command that is currently being autocompleted. May be null to indicate that we're completing thecommand itself.

**Kind**: instance property of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
<a name="AutocompleteMenu+open"></a>

### autocompleteMenu.open(text)
Checks if the input of the target text area contains a command that can be autocompleted. If so, the menu ispopulated and displayed above the input.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to complete. |

<a name="AutocompleteMenu+display"></a>

### autocompleteMenu.display(entries, [append], [done])
Replaces the current menu entries with the given array of elements. Elements exceeding the @see maxEntries limitare omitted and an overflow entry is displayed instead.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| entries | <code>Array.&lt;HTMLElement&gt;</code> |  | The menu entries to display. |
| [append] | <code>boolean</code> | <code>false</code> | Indicates whether the entries will be appended to the menu (instead of replacing  existing entries). Defaults to false. |
| [done] | <code>boolean</code> | <code>true</code> | Indicates whether the menu is complete. When false, a loading indicator will be  displayed. Defaults to true. |

<a name="AutocompleteMenu+focus"></a>

### autocompleteMenu.focus(event)
Handles focus changes between the text area and the menu.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | The input event that triggered the action. |

<a name="AutocompleteMenu+blur"></a>

### autocompleteMenu.blur()
Returns the focus from the menu to the text area.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
<a name="AutocompleteMenu+navigate"></a>

### autocompleteMenu.navigate(event)
Handles focus changes within the menu.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | The input event that triggered the action. |

<a name="AutocompleteMenu+close"></a>

### autocompleteMenu.close()
Closes the menu and resets its state.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
<a name="AutocompleteMenu+suggest"></a>

### autocompleteMenu.suggest(command)
Displays the given command in the suggestion area.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  

| Param | Type | Description |
| --- | --- | --- |
| command | <code>string</code> | The string containing the command to suggest. |

<a name="AutocompleteMenu+select"></a>

### autocompleteMenu.select(command)
Applies the given command to the chat message input and closes the menu.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  

| Param | Type | Description |
| --- | --- | --- |
| command | <code>string</code> | The command to select. |

<a name="AutocompleteMenu+resetSuggestion"></a>

### autocompleteMenu.resetSuggestion()
Clears the suggested command.

**Kind**: instance method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
<a name="AutocompleteMenu.initialize"></a>

### AutocompleteMenu.initialize()
Initialize the hook to attach autocomplete menus to chat message inputs.

**Kind**: static method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
**Access**: package  
<a name="AutocompleteMenu.stopEvent"></a>

### AutocompleteMenu.stopEvent(event)
Prevents all propagation for the given event so that no other handlers are called.

**Kind**: static method of [<code>AutocompleteMenu</code>](#AutocompleteMenu)  
**Access**: package  

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | The event to stop. |

<a name="ChatCommand"></a>

## ChatCommand
Represents a single chat command.

**Kind**: global class  

* [ChatCommand](#ChatCommand)
    * [new ChatCommand(data)](#new_ChatCommand_new)
    * [.names](#ChatCommand+names)
    * [.getDescription(alias, footer)](#ChatCommand+getDescription) ⇒ <code>string</code>
    * [.removeAlias(name)](#ChatCommand+removeAlias)
    * [.canInvoke()](#ChatCommand+canInvoke) ⇒ <code>boolean</code>
    * [.invoke(chat, parameters, messageData)](#ChatCommand+invoke) ⇒ <code>object</code> \| <code>Promise</code>
    * [.autocomplete(menu, alias, parameters)](#ChatCommand+autocomplete) ⇒ <code>Array.&lt;string&gt;</code>
    * [.resumeAutocomplete(generator)](#ChatCommand+resumeAutocomplete)
    * [.abortAutocomplete()](#ChatCommand+abortAutocomplete)

<a name="new_ChatCommand_new"></a>

### new ChatCommand(data)
Creates a new chat command from the given data.


| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | The data of the command. |
| data.name | <code>string</code> | The name that is used to invoke the command. |
| data.module | <code>string</code> | The ID of the module that registered the command. |
| data.aliases | <code>Array.&lt;string&gt;</code> | Aliases that can be used instead of the name. Defaults to an empty array. |
| data.description | <code>string</code> | The human readable description of the command. |
| data.icon | <code>string</code> | An HTML string containing the icon of the command. |
| [data.requiredRole] | <code>string</code> | A minimum role that is required to invoke the command. Defaults to "NONE" (everyone). |
| data.callback | <code>function</code> | An optional function that is called with the chat application that triggered  the command, its parameters and the original message data. The function can return null to apply FoundryVTT  core handling, an empty object to omit the message or a message data object that will be sent to the chat. |
| data.autocompleteCallback | <code>function</code> | An optional function that is called when a user is typing the  command with the menu that triggered the completion, the command's alias and its parameters. The function can  return an array of @see HTMLElement or an @see HTMLCollection that will be displayed in the autocomplete menu.  It may also return an asynchronous generator yielding element arrays. |
| [data.closeOnComplete] | <code>boolean</code> | Indicates that the menu should be closed after an entry is selected.  Defaults to true. |

<a name="ChatCommand+names"></a>

### chatCommand.names
Returns every name that this command can be invoked with (including aliases).

**Kind**: instance property of [<code>ChatCommand</code>](#ChatCommand)  
<a name="ChatCommand+getDescription"></a>

### chatCommand.getDescription(alias, footer) ⇒ <code>string</code>
Describes the command with predefined elements.

**Kind**: instance method of [<code>ChatCommand</code>](#ChatCommand)  
**Returns**: <code>string</code> - An HTML string containing a detailed command description.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| alias | <code>string</code> |  | The alias that should be described. |
| footer | <code>boolean</code> | <code>true</code> | Indicates whether a footer with other aliases and the module name will be displayed. |

<a name="ChatCommand+removeAlias"></a>

### chatCommand.removeAlias(name)
Removes an alias from this command. Note that this should only be called before registering the command since itdoes not remove the alias from the command registry.

**Kind**: instance method of [<code>ChatCommand</code>](#ChatCommand)  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The alias to remove. |

<a name="ChatCommand+canInvoke"></a>

### chatCommand.canInvoke() ⇒ <code>boolean</code>
Checks if the command can be invoked by the current user.

**Kind**: instance method of [<code>ChatCommand</code>](#ChatCommand)  
**Returns**: <code>boolean</code> - True if the command can be invoked by the current user, false otherwise.  
<a name="ChatCommand+invoke"></a>

### chatCommand.invoke(chat, parameters, messageData) ⇒ <code>object</code> \| <code>Promise</code>
Invokes the callback of the command.

**Kind**: instance method of [<code>ChatCommand</code>](#ChatCommand)  
**Returns**: <code>object</code> \| <code>Promise</code> - A chat message data object containing a new message. If omitted, no message will be sent. Alternatively, this may return a promise representing the result if the command's callback is asynchronous.  

| Param | Type | Description |
| --- | --- | --- |
| chat | <code>ChatLog</code> | The chat application that the command is being invoked from. |
| parameters | <code>string</code> | The parameters of the command (if any). |
| messageData | <code>object</code> | The data of the chat message invoking the command. |

<a name="ChatCommand+autocomplete"></a>

### chatCommand.autocomplete(menu, alias, parameters) ⇒ <code>Array.&lt;string&gt;</code>
Invokes the autocomplete callback of the command.

**Kind**: instance method of [<code>ChatCommand</code>](#ChatCommand)  
**Returns**: <code>Array.&lt;string&gt;</code> - A list of HTML strings or an HTMLCollection containing entries to complete the command or null when the command has no autocomplete callback.  

| Param | Type | Description |
| --- | --- | --- |
| menu | [<code>AutocompleteMenu</code>](#AutocompleteMenu) | The menu that the autocompletion is being invoked from. |
| alias | <code>string</code> | The alias that was used to initiate the autocomplete. |
| parameters | <code>string</code> | The parameters of the command (if any). |

<a name="ChatCommand+resumeAutocomplete"></a>

### chatCommand.resumeAutocomplete(generator)
Calls the next iteration of the given generator if it still belongs to the current autocomplete process. Theresult is displayed using the callback passed to @see autocomplete.

**Kind**: instance method of [<code>ChatCommand</code>](#ChatCommand)  

| Param | Type | Description |
| --- | --- | --- |
| generator | <code>AsyncGenerator</code> | The generator to fetch new entries with. |

<a name="ChatCommand+abortAutocomplete"></a>

### chatCommand.abortAutocomplete()
Aborts the current autocomplete process (if there is one) without displaying the result.

**Kind**: instance method of [<code>ChatCommand</code>](#ChatCommand)  
<a name="ChatCommands"></a>

## ChatCommands
Registry for chat commands and utility methods.

**Kind**: global class  

* [ChatCommands](#ChatCommands)
    * _instance_
        * [.commands](#ChatCommands+commands) : <code>Map.&lt;string, ChatCommand&gt;</code>
        * [.commandClass](#ChatCommands+commandClass) ⇒
        * [.register(command, [override])](#ChatCommands+register)
        * [.unregister(name)](#ChatCommands+unregister)
        * [.createCommandElement(command, content)](#ChatCommands+createCommandElement) ⇒ <code>HTMLElement</code>
        * [.createInfoElement(content)](#ChatCommands+createInfoElement) ⇒ <code>HTMLElement</code>
        * [.createSeparatorElement()](#ChatCommands+createSeparatorElement) ⇒ <code>HTMLElement</code>
        * [.createLoadingElement()](#ChatCommands+createLoadingElement) ⇒ <code>HTMLElement</code>
        * [.createOverflowElement()](#ChatCommands+createOverflowElement) ⇒ <code>HTMLElement</code>
        * [.isCommand(text)](#ChatCommands+isCommand) ⇒ <code>boolean</code>
        * [.parseCommand(text)](#ChatCommands+parseCommand) ⇒ <code>object</code>
        * [.handleMessage(chat, message, messageData)](#ChatCommands+handleMessage) ⇒ <code>boolean</code>
        * ~~[.registerCommand()](#ChatCommands+registerCommand)~~
        * ~~[.deregisterCommand()](#ChatCommands+deregisterCommand)~~
        * ~~[.createCommand()](#ChatCommands+createCommand)~~
        * ~~[.createCommandFromData()](#ChatCommands+createCommandFromData)~~
    * _static_
        * [.initialize()](#ChatCommands.initialize)

<a name="ChatCommands+commands"></a>

### chatCommands.commands : <code>Map.&lt;string, ChatCommand&gt;</code>
The map of currently registered commands and their aliases. Each alias has a separate entry that points to thesame @see ChatCommand instance.

**Kind**: instance property of [<code>ChatCommands</code>](#ChatCommands)  
<a name="ChatCommands+commandClass"></a>

### chatCommands.commandClass ⇒
Returns the class implementing a single chat command.

**Kind**: instance property of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: The @see ChatCommand class.  
<a name="ChatCommands+register"></a>

### chatCommands.register(command, [override])
Registers a single chat command using its data.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**See**: ChatCommand.constructor for valid fields.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| command | <code>object</code> \| [<code>ChatCommand</code>](#ChatCommand) |  | The command object to register. |
| [override] | <code>boolean</code> | <code>false</code> | Force the new command to override existing entries. Defaults to false. |

<a name="ChatCommands+unregister"></a>

### chatCommands.unregister(name)
Unregisters the given chat command and its aliases.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> \| [<code>ChatCommand</code>](#ChatCommand) | The name of the command or the command itself. |

<a name="ChatCommands+createCommandElement"></a>

### chatCommands.createCommandElement(command, content) ⇒ <code>HTMLElement</code>
Creates a selectable list entry for a single command.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>HTMLElement</code> - An HTML element containing a selectable command entry.  

| Param | Type | Description |
| --- | --- | --- |
| command | <code>string</code> | The command that the entry applies when it is selected. |
| content | <code>string</code> | An HTML string containing the displayed entry. |

<a name="ChatCommands+createInfoElement"></a>

### chatCommands.createInfoElement(content) ⇒ <code>HTMLElement</code>
Creates a non-selectable list entry for displaying additional information.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>HTMLElement</code> - An HTML element containing an informational entry.  

| Param | Type | Description |
| --- | --- | --- |
| content | <code>string</code> | An HTML string containing the displayed entry. |

<a name="ChatCommands+createSeparatorElement"></a>

### chatCommands.createSeparatorElement() ⇒ <code>HTMLElement</code>
Creates a list entry displaying a separator.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>HTMLElement</code> - An HTML element containing a separator entry.  
<a name="ChatCommands+createLoadingElement"></a>

### chatCommands.createLoadingElement() ⇒ <code>HTMLElement</code>
Creates a list entry displaying a loading indicator.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>HTMLElement</code> - An HTML element containing a loading entry.  
<a name="ChatCommands+createOverflowElement"></a>

### chatCommands.createOverflowElement() ⇒ <code>HTMLElement</code>
Creates an informational element indicating that more results are available.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>HTMLElement</code> - An HTML element containing the overflow hint.  
<a name="ChatCommands+isCommand"></a>

### chatCommands.isCommand(text) ⇒ <code>boolean</code>
Checks if the given text might contain a command. Note that this only checks if the first character is a commandcharacter, not that a command with that input actually exists.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>boolean</code> - True if the input might be a command, false otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to check. |

<a name="ChatCommands+parseCommand"></a>

### chatCommands.parseCommand(text) ⇒ <code>object</code>
Parses the given text to find a command and its parameters.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>object</code> - An object containing the command itself, the used alias and the parameters (or null if no command was found).  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | The text to search for commands in. |

<a name="ChatCommands+handleMessage"></a>

### chatCommands.handleMessage(chat, message, messageData) ⇒ <code>boolean</code>
Processes a chat message to check if it contains a command. If so, a permission check is performed, the commandis invoked and the invokeChatCommand hook is called. The result indicates whether a message will be sent.

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
**Returns**: <code>boolean</code> - False if the command was handled, undefined otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| chat | <code>ChatLog</code> | The chat that emitted the message. |
| message | <code>string</code> | The content of the message to send. |
| messageData | <code>object</code> | The data of the message to send. |

<a name="ChatCommands+registerCommand"></a>

### ~~chatCommands.registerCommand()~~
***Deprecated***

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
<a name="ChatCommands+deregisterCommand"></a>

### ~~chatCommands.deregisterCommand()~~
***Deprecated***

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
<a name="ChatCommands+createCommand"></a>

### ~~chatCommands.createCommand()~~
***Deprecated***

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
<a name="ChatCommands+createCommandFromData"></a>

### ~~chatCommands.createCommandFromData()~~
***Deprecated***

**Kind**: instance method of [<code>ChatCommands</code>](#ChatCommands)  
<a name="ChatCommands.initialize"></a>

### ChatCommands.initialize()
Attaches the API to the game instance and registers a hook to handle chat messages.

**Kind**: static method of [<code>ChatCommands</code>](#ChatCommands)  
**Access**: package  
