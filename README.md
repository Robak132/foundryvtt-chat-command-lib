# Chat Commander

This FoundryVTT module provides sophisticated autocompletion capabilities for commands and their parameters. It is meant to be used by other modules to keep a tidy (and conflict free) list of chat commands.

## Features

- Global command list with configurable autocompletion
- [Core command](#core-commands) autocompletion
- Command aliases
- Conflict handling with aliases and namespacing
- Role based command access
- [Asynchronous commands](#invocation-callback)
- [Autocompletion](#autocomplete-callback) for registered commands (including asynchronous autocompletion)
- [Command hook](#command-hook) to react to invocations and modify the resulting message

## Core commands

While this module's primary purpose is to provide centralized command handling for other modules, it does implement autocompletion for existing FoundryVTT commands.

### Whisper

For the core whisper command, you can type `@` to quickly initiate a private message. Autocompleting a user name will insert the full whisper syntax (also respecting spaces in the user's name).

When you then type a comma (`,`), you can append additional users. Again, the module will adjust the syntax as needed.

Keep in mind that `@` is only a convenience for autocompletion and not a known FoundryVTT command. Pasting something like `@GM How are you?` into the chat will **not** send a private message.

![Whisper example](./doc/whisper.gif)

### Roll

For rolling dice, the module provides some syntax guidelines. The level of detail depends on the amount of maximum entries in your (per user) configuration.

It also tracks your 5 recent rolls and suggests them proactively. This is independent of the roll mode (e.g. blind or public).

![Roll example](./doc/roll.gif)


# Developing commands

The following chapter covers various development scenarios for adding your own commands, reacting to invocations or patching existing functionality. All this is somewhat verbose, but don't worry: Most cases should be self-explanatory with nothing but the [registering commands](#registering-or-removing-commands) example.

Below this point, the term "module" refers to the module that you are developing, while "library" refers to the module documented here.

## Adding a dependency

Unless you have a fallback implementation for when the library is not active, you should specify it as a dependency in your module's manifest:

```json
{
    "id": "_chatcommands",
    "type": "module",
    "manifest": "https://gitlab.com/woodentavern/foundryvtt-chat-command-lib/-/raw/master/src/module.json",
    "compatibility": { "verified": "2.0.1" }
}
```

## Registering or removing commands

To add a simple command, simply call `game.chatCommands.register(data)` at any point after the initialization. If you want to be safe, you can use the `chatCommandsReady` hook that is called after the library's own commands (including FoundryVTT core commands) have been registered. 

The command's data may contain the following properties:
* **name**: *Required*. The name that is used to invoke the command. It is recommended (but not required) that this starts with a special character such as `/`.
    * Example: `{ name: "/test" }`
* **module**: *Required*. The ID of the module that registered the command. The source module name can be displayed in the command list and used for disambiguation in case of command name conflicts.
    * Example: `{ module: "_chatcommands" }`
* **aliases**: *Optional*. An array of aliases that can be used instead of the name. Commonly used for abbreviations. Aliases follow the same rules as the command name and *don't* have to start with the same special character.
    * Example: `{ aliases: ["/t", "%"] }`
* **description**: *Optional*. A short description of your command's functionality. It is recommended *not* to use more than 80 characters for this. If you need more complex instructions, use the autocomplete (see below) to display suggestions or informational entries.
    * Example: `{ description: game.i18n.localize("_chatcommands.testCommand.description") }`
* **icon**: *Optional*. An icon to display in front of the command name. This string can contain any HTML, but it is recommended to use a simple icon tag.
    * Example: `{ icon: "<i class='fas fa-dice-d20'></i>" }`
* **requiredRole**: *Optional*. The name of the lowest user role that is required to invoke the command. The default value is `NONE`, meaning that everyone can use the command. When the user does not have the role, the command will not be displayed in the autocomplete menu. Attempting to invoke the command will display a user friendly error. See the [user roles](https://foundryvtt.com/api/enums/foundry.CONST.USER_ROLES.html) constants for valid values.
* **callback**: *Optional*. A callback function that is invoked when a user enters the command. See the [invocation callback](#invocation-callback) section for details.
    * Example: `{ callback: (chat, parameters, messageData) => ({ content: parameters }) }`
* **autocompleteCallback**: *Optional*. A callback or generator function that is invoked when a user has entered the command. See the [autocomplete callback](#autocomplete-callback) section for details. If you don't provide a callback, the library will display the command description while typing its parameters.
    * Example: `{ autocompleteCallback: (menu, alias, parameters) => [game.chatCommands.createLoadingElement()] }`
* **closeOnComplete**: *Optional*. Indicates whether the autocomplete menu should be closed after an entry is selected. The default value is `true`. When set to `false`, the menu will stay open and call the autocomplete callback with the completed input.

Here's a fully functional usage example:
```javascript
Hooks.on("chatCommandsReady", commands => {
    commands.register({
        name: "/test",
        module: "_chatcommands",
        aliases: ["/t", "%"],
        description: game.i18n.localize("_chatcommands.testCommand.description"),
        icon: "<i class='fas fa-dice-d20'></i>",
        requiredRole: "NONE",
        callback: (chat, parameters, messageData) => ({ content: parameters }),
        autocompleteCallback: (menu, alias, parameters) => [game.chatCommands.createInfoElement("Enter a message.")],
        closeOnComplete: true
    });
});
```

A command can be removed by calling `game.chatCommands.unregister(name)`. It is usually not recommended to do this - if you want to explicitly replace an existing command, use the override parameter of the `register(data, true)` method instead. It is not necessary to do this for core commands as these can always be overwritten.

## Invocation callback

To run your own logic when the command is invoked, use the `callback` property to provide a function that takes:
* The `ChatLog` instance that triggered the command.
* The parameter text of the invocation, which is everything after the first space (except for single character aliases, where the space is omitted).
* The data of the chat message (user and speaker information).

The return value controls what happens after completing the callback. Returning an empty object (or message data without content) will prevent further interaction. You can also return nothing (`undefined`) or `null` to use FoundryVTT's command handling.

```javascript
game.chatCommands.register({
    name: "/siege",
    module: "_chatcommands",
    description: "Conditionally uses core command handling.",
    callback: (chat, parameters, messageData) => {
        if (parameters.toLowerCase().includes("trebuchet")) {
            new Roll("3d20").toMessage(messageData);
            return {}; // Do nothing.
        } else {
            return; // Use core handling (this would fail because siege is not a core command).
        }
    }
});
```

If you do want to emit a message, you can return an object that will be used to create a `ChatMessage`.

> See the [ChatMessageData](https://foundryvtt.com/api/v9/data.ChatMessageData.html) documentation for valid properties (v9 link, v10 doesn't seem to have accurate documentation for this).

The object returned by the callback will be merged into the existing message data (the same that is passed in as a parameter), so you *don't* need to transfer user & speaker information yourself. In order to override those fields explicitly, you can either set them in your return value or modify the message data directly.

```javascript
game.chatCommands.register({
    name: "/git-blame-someone-else",
    module: "_chatcommands",
    description: "Make sure that your name isn't on the commit.",
    callback: (chat, parameters, messageData) => {
        // Change the existing message data...
        messageData.speaker = ChatMessage._getSpeakerFromUser({ user: game.users.find(u => u !== game.user) });

        // ... or merge it from the returned object.
        return {
            content: parameters,
            user: game.users.find(u => u !== game.user),
            type: CONST.CHAT_MESSAGE_TYPES.OOC
        };
    }
});
```

You can also use an asynchronous function as a callback. This works the same way as a regular function, except that you cannot use core command handling as a fallback. A loading indicator will be displayed over the chat message input until the promise is resolved or rejected.

```javascript
game.chatCommands.register({
    name: "/procrastinate",
    module: "_chatcommands",
    description: "Send a message... later.",
    callback: async (chat, parameters, messageData) => {
        const delay = Math.floor(Math.random() * 9001 + 1000);
        await new Promise(resolve => setTimeout(resolve, delay));
        return { content: parameters };
    }
});
```

![Asynchronous command example](./doc/procrastinate.gif)

## Command hook

In order to give other developers a chance to intercept and modify commands, the library calls the `invokeChatCommand` hook after invoking a command, but before its results are sent to the chat. The hook receives:
* The `ChatLog` instance that triggered the command.
* The `ChatCommand` instance that was invoked.
* The parameter text of the invocation.
* The result of the invocation (or an empty object if there was no result).
* An options object containing additional information.

This can be used to change the message data before the message is created, e.g. to append content:

```javascript
game.chatCommands.register({
    name: "/fellowship",
    module: "_chatcommands",
    description: "Appends a fellowship declaration to the end of the given text.",
    callback: (chat, parameters, messageData) => ({
        content: parameters + "\nAnd my bow!"
    })
});
Hooks.on("invokeChatCommand", (chat, command, parameters, result, options) => {
    if (command.name === "/fellowship") result.content += "\nAnd my axe!";
});
```

It is possible to create a message even though the command decided not to send one by adding a `content` string to the `result`. Similarly, you can prevent sending a message by removing (or emptying) the content. Be aware that doing so may cause the message to revert to FoundryVTT's default behavior. To prevent this, you may check `options.handleCore` and set it to `false`:

```javascript
game.chatCommands.register({
    name: "/nothing",
    module: "_chatcommands",
    description: "Doesn't do anything and lets FoundryVTT handle the message."
});
Hooks.on("invokeChatCommand", (chat, command, parameters, result, options) => {
    // Send a message even though the command itself didn't want to.
    if (command.name === "/nothing") result.content = "It now sends its own message.";
});
Hooks.on("invokeChatCommand", (chat, command, parameters, result, options) => {
    // Nevermind, don't send a message at all.
    if (command.name === "/nothing") {
        result.content = "";
        options.handleCore = false; // Without this, FoundryVTT would handle the result.
    }
});
```

This approach also works for asynchronous callbacks, with the exception that they can never use core handling (even if a hook sets the option to `true`).

## Autocomplete callback

The `autocompleteCallback` function is optional and may be used to display suggestions or information for the current command. The callback will receive:
* The menu controlling the autocompletion process. Its state information can be used to refine the results, e.g. the maximum amount of entries (`menu.maxEntries`) or the current visibility (`menu.visible`).
* The alias that was used.
* The parameters (everything after the first space or single character alias).

You can use the API's `create*Element` methods to quickly create valid entries with HTML content.

```javascript
game.chatCommands.register({
    name: "/ac-simple",
    module: "_chatcommands",
    description: "Demonstrates a simple autocomplete callback.",
    autocompleteCallback: (menu, alias, parameters) => {
        const entries = [
            game.chatCommands.createCommandElement(`${alias} Parameter`, "Suggestion: <strong>Parameter</strong>"),
            game.chatCommands.createCommandElement(`/redirect`, "Use another <em>command</em>"),
            game.chatCommands.createSeparatorElement(),
            game.chatCommands.createInfoElement("Did you know? Pressing ESC will close the menu."),
            game.chatCommands.createLoadingElement()
        ];
        entries.length = Math.min(entries.length, menu.maxEntries);
        return entries;
    }
});
```

![Autocompletion element example](./doc/ac-simple.gif)

The returned commands should usually start with the provided alias, but they don't need to. You can also complete with other aliases (e.g. to teach the user about abbreviations) or entirely different commands. By default, a command will be automatically suggested when there is exactely one in the menu. You can change this behavior by calling `menu.suggest(command)` before returning your entries. This also works when you are returning an empty array (which will close the menu, but still display your suggestion).

> The menu can display arbitrary HTML elements. However, command entries must be in the form `<li class="command" tab-index="0" data-command="the-command">` in order to be used as suggestion (dataset and class) and for focus navigation (tab index). Therefore, it is recommended to use the provided utility methods and add your own markup to the content parameter.

Note that clipping the entries is optional and only recommended when many of them are created in a loop. The sample code will not display an overflow entry - to do that, either use `menu.maxEntries + 1` (prompting the library to create it for you) or add it yourself using `entries.unshift(game.chatCommands.createOverflowElement())`.

This kind of callback should be very performance friendly. Asynchronous calls or lookups that can take longer than 100 milliseconds should use an async generator instead:

```javascript
game.chatCommands.register({
    name: "/ac-generator",
    module: "_chatcommands",
    description: "Demonstrates a slow or asynchronous autocomplete callback.",
    autocompleteCallback: async function* (menu, alias, parameters) {
        for (let i = 1; i < menu.maxEntries + 10; i++) {
            // Wait for 0.1 seconds.
            await new Promise(resolve => setTimeout(resolve, 100));

            // Yield an element.
            yield [game.chatCommands.createCommandElement(alias + " " + i, "Command " + i)];
        }
    }
});
```

![Generator completion example](./doc/ac-generator.gif)

The [generator syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function*#description) is not strictly required, but recommended. The library will automatically display a loading indicator while the generator is running. The amount of elements yielded can be more than one, but should be relatively small (one yield per waiting cycle is common).

When yielding only a few elements, you don't need to worry about the amount of entries. The library will simply abort your generator after the maximum has been reached and display an overflow entry if the generator didn't complete by itself. It will also be aborted when the user changes the input or closes the menu.

Note that there is a 300 millisecond startup delay to avoid searches while the user is still typing. This delay happens after the generator is created, but before its first invocation.


## Advanced customization

This section is for deeper integration with the library to cover more complicated use cases. I take no responsibility for things you do with the API methods not covered by the previous sections. Feel free to open an issue (or a pull request) if you need me to expose additional functionality to cover your use case.

Refer to the [API reference](API.md) for details on specific methods.

### Changing commands

If you need to patch an existing command without replacing it, you can do so using the `game.chatCommands.commands` map, which contains a dictionary of every alias and their commands. It is sufficient to change one alias as they share the same instance.

Keep in mind that you always need to check the map for existence first. Do not assume that a command is present - even core commands can be disabled based on the user's configuration.

### Command behavior

An instance of a class derived from `ChatCommand` can be provided to the `ChatCommands.register(command)` method. You can extend the existing command class that is available under `window._chatcommands.ChatCommand` when the `init` hook is called (the library sets up the class as soon as the script is loaded).

Alternatively, use `await import("../_chatcommands/scripts/chatCommand.mjs")` (assuming you're in your module's root) to access the ES6 script directly. I recommend using the global class whenever possible to avoid a hard dependency on the file location.

The most common places to override are the `canInvoke` method to impose additional restrictions or the `invoke` method to execute custom behavior. Take care when overriding the `autocomplete` method as you may also have to adjust the `resumeAutocomplete` implementation that deals with asynchronous generators. When passing in your own menu, you must implement a `display(entries, append, done)` method and a `maxEntries` property. Third party commands may also access other functionality, so I recommend the use of the library's menu implementation.

### Autocomplete menu

The autocompletion can be accessed using `window.ui.chat.autocompleteMenu`. Make sure to access the correct chat instance - for example, there could be another menu in `window.ui.chat._popout.autocompleteMenu`. The library's hooks and callbacks always provide the relevant instance.

The `maxEntries` property is loaded from the library's settings when the instance is created and may be changed at any time (e.g. if you want to guarantee that multiple informational entries are displayed). Make sure to restore the original value when you are done.

This instance can also be used to programmatically open the menu using `open(text)`. Adding entries to an already visible menu is done using the `display(entries, true)` method. Make sure to also set the `done` parameter of this call in asynchronous environments.

Keybind behavior can be adjusted by overriding `focus(event)` (from chat to menu), `navigate(event)` (within the menu) or `blur()` (from menu to chat). Suggestions are extensible with `suggest(text)`, `resetSuggestion()` and `select(text)`.
 